package Lab1;

public interface Shape {
    abstract void inputData();
    abstract void calculateArea();
}
