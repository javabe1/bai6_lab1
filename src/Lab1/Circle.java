package Lab1;

import java.util.Scanner;

public class Circle implements Shape{
    private double r;
    public Circle() {
    }
    @Override
    public void inputData()
    {
        Scanner scanner = new Scanner(System.in);
        this.r = scanner.nextInt();
    }
    public void calculateArea()
    {
        System.out.println("Dien tich hinh tron la: " + 3.14*this.r*this.r);
    }
}
