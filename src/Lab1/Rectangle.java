package Lab1;

import java.util.Scanner;

public class Rectangle implements Shape{
    private int d,r;
    public Rectangle() {
    }

    @Override
    public void inputData() {
        Scanner scanner = new Scanner(System.in);
        this.d = scanner.nextInt();
        this.r = scanner.nextInt();
    }

    public void calculateArea()
    {
        System.out.println("Dien tich HCN la: " + this.d*this.r);
    }
}
