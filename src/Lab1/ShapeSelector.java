package Lab1;

import java.util.Scanner;

public class ShapeSelector {
    Scanner scanner = new Scanner(System.in);
    Shape circle = new Circle();
    Shape rectangle = new Rectangle();
    Shape tritangle = new Tritangle();
    public ShapeSelector() {
    }
    private int x = 0;
    public void Chon()
    {
        while (true){
        System.out.println("Lua chon hinh muon tinh dien tich: " + "\n" + "- 1. Hinh tron" + "\n" + "- 2. Hinh chu nhat" + "\n" + "- 3. Hinh tam giac");
        this.x = scanner.nextInt();
        switch (x) {
            case 1:
                System.out.println("-----Tinh dien tich hinh tron-----");
                System.out.println("Nhap vao ban kinh:");
                circle.inputData();
                circle.calculateArea();
                break;
            case 2:
                System.out.println("-----Tinh dien tich hinh chu nhat-----");
                System.out.println("Nhap vao chieu dai va rong:");
                rectangle.inputData();
                rectangle.calculateArea();
                break;
            case 3:
                System.out.println("-----Tinh dien tich hinh tam giac-----");
                System.out.println("Nhap vao chieu cao va canh:");
                tritangle.inputData();
                tritangle.calculateArea();
            }
        }
    }
}
